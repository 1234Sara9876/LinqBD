﻿namespace FormEditar
{
    partial class FormEditar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditar));
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDepEdit = new System.Windows.Forms.TextBox();
            this.tbNomeFuncEdit = new System.Windows.Forms.TextBox();
            this.tbIDFuncEdit = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnForward
            // 
            this.btnForward.Image = ((System.Drawing.Image)(resources.GetObject("btnForward.Image")));
            this.btnForward.Location = new System.Drawing.Point(165, 264);
            this.btnForward.Margin = new System.Windows.Forms.Padding(2);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(26, 36);
            this.btnForward.TabIndex = 22;
            this.btnForward.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(76, 264);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(23, 36);
            this.btnBack.TabIndex = 21;
            this.btnBack.UseVisualStyleBackColor = true;
            // 
            // btnGravar
            // 
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.Location = new System.Drawing.Point(332, 264);
            this.btnGravar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(33, 36);
            this.btnGravar.TabIndex = 23;
            this.btnGravar.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Departamento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Nome completo";
            // 
            // tbDepEdit
            // 
            this.tbDepEdit.Location = new System.Drawing.Point(128, 222);
            this.tbDepEdit.Name = "tbDepEdit";
            this.tbDepEdit.Size = new System.Drawing.Size(187, 20);
            this.tbDepEdit.TabIndex = 29;
            // 
            // tbNomeFuncEdit
            // 
            this.tbNomeFuncEdit.Location = new System.Drawing.Point(22, 105);
            this.tbNomeFuncEdit.Multiline = true;
            this.tbNomeFuncEdit.Name = "tbNomeFuncEdit";
            this.tbNomeFuncEdit.Size = new System.Drawing.Size(296, 63);
            this.tbNomeFuncEdit.TabIndex = 28;
            // 
            // tbIDFuncEdit
            // 
            this.tbIDFuncEdit.BackColor = System.Drawing.SystemColors.Window;
            this.tbIDFuncEdit.Location = new System.Drawing.Point(180, 19);
            this.tbIDFuncEdit.Margin = new System.Windows.Forms.Padding(2);
            this.tbIDFuncEdit.Name = "tbIDFuncEdit";
            this.tbIDFuncEdit.ReadOnly = true;
            this.tbIDFuncEdit.Size = new System.Drawing.Size(40, 20);
            this.tbIDFuncEdit.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "ID Funcionário";
            // 
            // FormEditar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 301);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDepEdit);
            this.Controls.Add(this.tbNomeFuncEdit);
            this.Controls.Add(this.tbIDFuncEdit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.btnBack);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormEditar";
            this.Text = "Editar Funcionários";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDepEdit;
        private System.Windows.Forms.TextBox tbNomeFuncEdit;
        private System.Windows.Forms.TextBox tbIDFuncEdit;
        private System.Windows.Forms.Label label1;
    }
}

