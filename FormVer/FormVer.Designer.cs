﻿namespace FormVer
{
    partial class FormVer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVer));
            this.dgviewFunc = new System.Windows.Forms.DataGridView();
            this.libFiltro = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFunc = new System.Windows.Forms.ComboBox();
            this.lblTodosAlunos = new System.Windows.Forms.Label();
            this.btnFechar4 = new System.Windows.Forms.Button();
            this.tbFunc = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgviewFunc)).BeginInit();
            this.SuspendLayout();
            // 
            // dgviewFunc
            // 
            this.dgviewFunc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgviewFunc.Location = new System.Drawing.Point(22, 288);
            this.dgviewFunc.Name = "dgviewFunc";
            this.dgviewFunc.RowTemplate.Height = 24;
            this.dgviewFunc.Size = new System.Drawing.Size(530, 133);
            this.dgviewFunc.TabIndex = 36;
            // 
            // libFiltro
            // 
            this.libFiltro.FormattingEnabled = true;
            this.libFiltro.Location = new System.Drawing.Point(332, 38);
            this.libFiltro.Name = "libFiltro";
            this.libFiltro.Size = new System.Drawing.Size(220, 225);
            this.libFiltro.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(382, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Filtro de funcionários";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 190);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Departamento";
            // 
            // cbFunc
            // 
            this.cbFunc.FormattingEnabled = true;
            this.cbFunc.Location = new System.Drawing.Point(22, 38);
            this.cbFunc.Name = "cbFunc";
            this.cbFunc.Size = new System.Drawing.Size(236, 21);
            this.cbFunc.TabIndex = 30;
            // 
            // lblTodosAlunos
            // 
            this.lblTodosAlunos.AutoSize = true;
            this.lblTodosAlunos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTodosAlunos.Location = new System.Drawing.Point(19, 18);
            this.lblTodosAlunos.Name = "lblTodosAlunos";
            this.lblTodosAlunos.Size = new System.Drawing.Size(132, 13);
            this.lblTodosAlunos.TabIndex = 29;
            this.lblTodosAlunos.Text = "Todos os funcionários";
            // 
            // btnFechar4
            // 
            this.btnFechar4.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar4.Image")));
            this.btnFechar4.Location = new System.Drawing.Point(577, 418);
            this.btnFechar4.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar4.Name = "btnFechar4";
            this.btnFechar4.Size = new System.Drawing.Size(36, 33);
            this.btnFechar4.TabIndex = 37;
            this.btnFechar4.UseVisualStyleBackColor = true;
            // 
            // tbFunc
            // 
            this.tbFunc.Location = new System.Drawing.Point(156, 182);
            this.tbFunc.Name = "tbFunc";
            this.tbFunc.Size = new System.Drawing.Size(100, 20);
            this.tbFunc.TabIndex = 38;
            // 
            // FormVer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 453);
            this.ControlBox = false;
            this.Controls.Add(this.tbFunc);
            this.Controls.Add(this.btnFechar4);
            this.Controls.Add(this.dgviewFunc);
            this.Controls.Add(this.libFiltro);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbFunc);
            this.Controls.Add(this.lblTodosAlunos);
            this.Name = "FormVer";
            this.Text = "Consultar funcionários";
            ((System.ComponentModel.ISupportInitialize)(this.dgviewFunc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgviewFunc;
        private System.Windows.Forms.ListBox libFiltro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFunc;
        private System.Windows.Forms.Label lblTodosAlunos;
        private System.Windows.Forms.Button btnFechar4;
        private System.Windows.Forms.TextBox tbFunc;
    }
}

