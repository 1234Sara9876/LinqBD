﻿namespace FormCriar
{
    partial class FormCriar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCriar));
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnFechar3 = new System.Windows.Forms.Button();
            this.tbIDFunc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNomeFunc = new System.Windows.Forms.TextBox();
            this.tbDep = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGravar
            // 
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.Location = new System.Drawing.Point(287, 284);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(40, 33);
            this.btnGravar.TabIndex = 21;
            this.btnGravar.UseVisualStyleBackColor = true;
            // 
            // btnFechar3
            // 
            this.btnFechar3.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar3.Image")));
            this.btnFechar3.Location = new System.Drawing.Point(341, 284);
            this.btnFechar3.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar3.Name = "btnFechar3";
            this.btnFechar3.Size = new System.Drawing.Size(36, 33);
            this.btnFechar3.TabIndex = 20;
            this.btnFechar3.UseVisualStyleBackColor = true;
            // 
            // tbIDFunc
            // 
            this.tbIDFunc.BackColor = System.Drawing.SystemColors.Window;
            this.tbIDFunc.Location = new System.Drawing.Point(208, 33);
            this.tbIDFunc.Margin = new System.Windows.Forms.Padding(2);
            this.tbIDFunc.Name = "tbIDFunc";
            this.tbIDFunc.ReadOnly = true;
            this.tbIDFunc.Size = new System.Drawing.Size(40, 20);
            this.tbIDFunc.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "ID Funcionário";
            // 
            // tbNomeFunc
            // 
            this.tbNomeFunc.Location = new System.Drawing.Point(50, 119);
            this.tbNomeFunc.Multiline = true;
            this.tbNomeFunc.Name = "tbNomeFunc";
            this.tbNomeFunc.Size = new System.Drawing.Size(296, 63);
            this.tbNomeFunc.TabIndex = 22;
            // 
            // tbDep
            // 
            this.tbDep.Location = new System.Drawing.Point(156, 236);
            this.tbDep.Name = "tbDep";
            this.tbDep.Size = new System.Drawing.Size(187, 20);
            this.tbDep.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Nome completo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Departamento";
            // 
            // FormCriar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 317);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDep);
            this.Controls.Add(this.tbNomeFunc);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.btnFechar3);
            this.Controls.Add(this.tbIDFunc);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormCriar";
            this.Text = "Novo Funcionário";
            this.Load += new System.EventHandler(this.FormCriar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnFechar3;
        private System.Windows.Forms.TextBox tbIDFunc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNomeFunc;
        private System.Windows.Forms.TextBox tbDep;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

