﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD
{
    class Program
    {
        static void Main2(string[] args)
        {
            DataClasses1DataContext dc = new DataClasses1DataContext();

            //carregar lista de funcionários
            var lista = from Funcionario in dc.Funcionarios orderby Funcionario.Nome select Funcionario;
           
            foreach (Funcionario func in lista)
            {
                Console.WriteLine("ID:" + func.ID);
                Console.WriteLine("Nome:" + func.Nome);
                Console.WriteLine("Departamento:" + func.Departamento);
                Console.WriteLine();
            }
            Console.WriteLine("Existem de momento {0} funcionários", lista.Count());
            Console.ReadKey();
        }
    }
}
