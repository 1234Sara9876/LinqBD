﻿namespace FormRemover
{
    partial class FormRemover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRemover));
            this.btnFechar5 = new System.Windows.Forms.Button();
            this.btnGravar2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbRemover = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnFechar5
            // 
            this.btnFechar5.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar5.Image")));
            this.btnFechar5.Location = new System.Drawing.Point(297, 224);
            this.btnFechar5.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar5.Name = "btnFechar5";
            this.btnFechar5.Size = new System.Drawing.Size(33, 36);
            this.btnFechar5.TabIndex = 25;
            this.btnFechar5.UseVisualStyleBackColor = true;
            // 
            // btnGravar2
            // 
            this.btnGravar2.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar2.Image")));
            this.btnGravar2.Location = new System.Drawing.Point(249, 224);
            this.btnGravar2.Margin = new System.Windows.Forms.Padding(2);
            this.btnGravar2.Name = "btnGravar2";
            this.btnGravar2.Size = new System.Drawing.Size(33, 36);
            this.btnGravar2.TabIndex = 24;
            this.btnGravar2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(64, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 19);
            this.label1.TabIndex = 23;
            this.label1.Text = "Seleccionar o funcionário";
            // 
            // cbRemover
            // 
            this.cbRemover.FormattingEnabled = true;
            this.cbRemover.Location = new System.Drawing.Point(43, 120);
            this.cbRemover.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemover.Name = "cbRemover";
            this.cbRemover.Size = new System.Drawing.Size(244, 21);
            this.cbRemover.TabIndex = 22;
            // 
            // FormRemover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 261);
            this.ControlBox = false;
            this.Controls.Add(this.btnFechar5);
            this.Controls.Add(this.btnGravar2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbRemover);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormRemover";
            this.Text = "Apagar Funcionário";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFechar5;
        private System.Windows.Forms.Button btnGravar2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbRemover;
    }
}

