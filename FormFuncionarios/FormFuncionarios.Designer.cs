﻿namespace FormFuncionarios
{
    partial class FormFuncionarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFuncionarios));
            this.btnFechar2 = new System.Windows.Forms.Button();
            this.btnDelete2 = new System.Windows.Forms.Button();
            this.btnUpdate2 = new System.Windows.Forms.Button();
            this.btnRead2 = new System.Windows.Forms.Button();
            this.btnCreate2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFechar2
            // 
            this.btnFechar2.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar2.Image")));
            this.btnFechar2.Location = new System.Drawing.Point(365, 335);
            this.btnFechar2.Name = "btnFechar2";
            this.btnFechar2.Size = new System.Drawing.Size(48, 41);
            this.btnFechar2.TabIndex = 9;
            this.btnFechar2.UseVisualStyleBackColor = true;
            // 
            // btnDelete2
            // 
            this.btnDelete2.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete2.Image")));
            this.btnDelete2.Location = new System.Drawing.Point(241, 190);
            this.btnDelete2.Name = "btnDelete2";
            this.btnDelete2.Size = new System.Drawing.Size(75, 73);
            this.btnDelete2.TabIndex = 8;
            this.btnDelete2.UseVisualStyleBackColor = true;
            // 
            // btnUpdate2
            // 
            this.btnUpdate2.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate2.Image")));
            this.btnUpdate2.Location = new System.Drawing.Point(102, 190);
            this.btnUpdate2.Name = "btnUpdate2";
            this.btnUpdate2.Size = new System.Drawing.Size(75, 73);
            this.btnUpdate2.TabIndex = 7;
            this.btnUpdate2.UseVisualStyleBackColor = true;
            // 
            // btnRead2
            // 
            this.btnRead2.Image = ((System.Drawing.Image)(resources.GetObject("btnRead2.Image")));
            this.btnRead2.Location = new System.Drawing.Point(241, 56);
            this.btnRead2.Name = "btnRead2";
            this.btnRead2.Size = new System.Drawing.Size(75, 69);
            this.btnRead2.TabIndex = 6;
            this.btnRead2.UseVisualStyleBackColor = true;
            // 
            // btnCreate2
            // 
            this.btnCreate2.Image = ((System.Drawing.Image)(resources.GetObject("btnCreate2.Image")));
            this.btnCreate2.Location = new System.Drawing.Point(102, 56);
            this.btnCreate2.Name = "btnCreate2";
            this.btnCreate2.Size = new System.Drawing.Size(75, 69);
            this.btnCreate2.TabIndex = 5;
            this.btnCreate2.UseVisualStyleBackColor = true;
            // 
            // FormFuncionarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 376);
            this.ControlBox = false;
            this.Controls.Add(this.btnFechar2);
            this.Controls.Add(this.btnDelete2);
            this.Controls.Add(this.btnUpdate2);
            this.Controls.Add(this.btnRead2);
            this.Controls.Add(this.btnCreate2);
            this.Name = "FormFuncionarios";
            this.Text = "Funcionários";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFechar2;
        private System.Windows.Forms.Button btnDelete2;
        private System.Windows.Forms.Button btnUpdate2;
        private System.Windows.Forms.Button btnRead2;
        private System.Windows.Forms.Button btnCreate2;
    }
}

