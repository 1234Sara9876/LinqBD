﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormEmpresa
{
    public partial class Funcionarios : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        private FormEmpresa form;

        private Criar fCriar;
        private Ver fVer;
        private Editar fEditar;
        private Remover fRemover;

        public Funcionarios()
        {
            InitializeComponent();
        }

        private void btnCreate2_Click(object sender, EventArgs e)
        {
            fCriar = new Criar();
            fCriar.Show();
        }

        private void btnRead2_Click(object sender, EventArgs e)
        {
            fVer = new Ver();
            fVer.Show();
        }

        private void btnUpdate2_Click(object sender, EventArgs e)
        {
            fEditar = new Editar();
            fEditar.Show();

        }

        private void btnDelete2_Click(object sender, EventArgs e)
        {
            fRemover = new Remover();
            fRemover.Show();

        }

        private void btnFechar2_Click(object sender, EventArgs e)
        {
            form = new FormEmpresa();
            this.Close();
            form.Show();
        }
    }
}
