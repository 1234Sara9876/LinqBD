﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormEmpresa
{
    public partial class Editar : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        int index = 1;

        public Editar()
        {
            InitializeComponent();
            MostraLista();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (index > 1)
            {
                index--;
                MostraLista();
            }
        }

        private void btnForward_Click(object sender, EventArgs e)
        {
            var lista = from Funcionario in dc.Funcionarios select Funcionario;
            if (index < lista.Count() )//pq n é um array, a lista começa em 1 
            {
                index++;
                MostraLista();
            }
        }

        private void MostraLista()
        {
        var lista = from Funcionario in dc.Funcionarios select Funcionario;

        if (lista.Count() == 0)
        {
            MessageBox.Show("A lista não tem nenhum funcionário");
            return;
        }
        else
            foreach ( Funcionario func in lista)
                if (func.ID == index)
                {
                tbIDFuncEdit.Text= func.ID.ToString();
                tbNomeFuncEdit.Text = func.Nome;
                tbDepEdit.Text = func.Departamento;
                }
        }

        private void btnGravar3_Click(object sender, EventArgs e)
        {
            Funcionario f = new Funcionario();
            var lista = from Funcionario in dc.Funcionarios
                        where Funcionario.ID == index select Funcionario;

            f = lista.Single();
            f.Nome = tbNomeFuncEdit.Text;
            f.Departamento = tbDepEdit.Text;

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
            MessageBox.Show("Dados de funcionário alterados com sucesso");
            Close();
            }
        }
   
    }

