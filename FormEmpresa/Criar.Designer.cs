﻿namespace FormEmpresa
{
    partial class Criar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criar));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDep = new System.Windows.Forms.TextBox();
            this.tbNomeFunc = new System.Windows.Forms.TextBox();
            this.btnGravar2 = new System.Windows.Forms.Button();
            this.btnFechar3 = new System.Windows.Forms.Button();
            this.tbIDFunc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Departamento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Nome completo";
            // 
            // tbDep
            // 
            this.tbDep.Location = new System.Drawing.Point(146, 242);
            this.tbDep.Name = "tbDep";
            this.tbDep.Size = new System.Drawing.Size(187, 20);
            this.tbDep.TabIndex = 31;
            // 
            // tbNomeFunc
            // 
            this.tbNomeFunc.Location = new System.Drawing.Point(40, 125);
            this.tbNomeFunc.Multiline = true;
            this.tbNomeFunc.Name = "tbNomeFunc";
            this.tbNomeFunc.Size = new System.Drawing.Size(296, 63);
            this.tbNomeFunc.TabIndex = 30;
            // 
            // btnGravar2
            // 
            this.btnGravar2.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar2.Image")));
            this.btnGravar2.Location = new System.Drawing.Point(279, 309);
            this.btnGravar2.Name = "btnGravar2";
            this.btnGravar2.Size = new System.Drawing.Size(40, 33);
            this.btnGravar2.TabIndex = 29;
            this.btnGravar2.UseVisualStyleBackColor = true;
            this.btnGravar2.Click += new System.EventHandler(this.btnGravar2_Click);
            // 
            // btnFechar3
            // 
            this.btnFechar3.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar3.Image")));
            this.btnFechar3.Location = new System.Drawing.Point(333, 309);
            this.btnFechar3.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar3.Name = "btnFechar3";
            this.btnFechar3.Size = new System.Drawing.Size(36, 33);
            this.btnFechar3.TabIndex = 28;
            this.btnFechar3.UseVisualStyleBackColor = true;
            this.btnFechar3.Click += new System.EventHandler(this.btnFechar3_Click);
            // 
            // tbIDFunc
            // 
            this.tbIDFunc.BackColor = System.Drawing.SystemColors.Window;
            this.tbIDFunc.Location = new System.Drawing.Point(198, 39);
            this.tbIDFunc.Margin = new System.Windows.Forms.Padding(2);
            this.tbIDFunc.Name = "tbIDFunc";
            this.tbIDFunc.ReadOnly = true;
            this.tbIDFunc.Size = new System.Drawing.Size(40, 20);
            this.tbIDFunc.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(91, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "ID Funcionário";
            // 
            // Criar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 342);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDep);
            this.Controls.Add(this.tbNomeFunc);
            this.Controls.Add(this.btnGravar2);
            this.Controls.Add(this.btnFechar3);
            this.Controls.Add(this.tbIDFunc);
            this.Controls.Add(this.label1);
            this.Name = "Criar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Criar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDep;
        private System.Windows.Forms.TextBox tbNomeFunc;
        private System.Windows.Forms.Button btnGravar2;
        private System.Windows.Forms.Button btnFechar3;
        private System.Windows.Forms.TextBox tbIDFunc;
        private System.Windows.Forms.Label label1;
    }
}