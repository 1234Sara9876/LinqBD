﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormEmpresa
{
    public partial class Departamentos : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        private FormEmpresa form;

        public Departamentos()
        {
            InitializeComponent();
            InitCombo();
        }

        private void InitCombo()
        {
            var lista = from Departamento in dc.Departamentos select Departamento;
            cbDep.DataSource = lista;
            cbDep.DisplayMember = "Departamento1";
        }

        private void btnCreate1_Click(object sender, EventArgs e)
        {
            tbSigla.Enabled = true;
            tbNovoDep.Enabled = true;
            
            
            btnGravar1.Enabled = true;
        }

        private void tbStatusDep_TextChanged(object sender, EventArgs e)
        {
            
            btnGravar1.Enabled = true;
           
        }

        private void btnGravar1_Click(object sender, EventArgs e)
        {
            if (tbStatusDep.Text != "Ativo")
            {
                if (cbDep.SelectedIndex == -1)
                {
                    MessageBox.Show("Indique o departamento");
                    return;
                }
                if (tbStatusDep.Text != "Activo" && tbStatusDep.Text != "Inativo" && tbStatusDep.Text != "Inactivo")
                {
                    MessageBox.Show("Indique se o departamento fica ativo ou inativo");
                    return;
                }
                Departamento dep = new Departamento();
                var lista = from Departamento in dc.Departamentos
                            where Departamento.Departamento1 == cbDep.Text
                            select Departamento;

                dep = lista.Single();

                var verifLista = from Funcionario in dc.Funcionarios
                                 where Funcionario.Departamento == dep.Sigla
                                 select Funcionario;
                if (verifLista.Count()== 0)
                {
                    dep.Departamento1 = tbStatusDep.Text;
                    try
                    {
                        dc.SubmitChanges();
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message);
                    }
                    MessageBox.Show("Departamento alterado com sucesso");
                    Close();
                }
                else
                {
                    MessageBox.Show("Não pode desativar o departamento, porque contém funcionários ");
                    return;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(tbSigla.Text))
                {
                    MessageBox.Show("Introduza a sigla do departamento");
                    return;
                }
                if (string.IsNullOrEmpty(tbNovoDep.Text))
                {
                    MessageBox.Show("Introduza o nome do departamento");
                    return;
                }
                Departamento dep = new Departamento
                {
                    Sigla = tbSigla.Text,
                    Departamento1 = tbNovoDep.Text
                };

                dc.Departamentos.InsertOnSubmit(dep);
                try
                {
                    dc.SubmitChanges();
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }
                MessageBox.Show("Novo departamento gravado com sucesso");
                tbSigla.Enabled = false;
                tbNovoDep.Enabled = false;
            }
        }

        private void btnFechar1_Click(object sender, EventArgs e)
        {
            form = new FormEmpresa();
            this.Close();
            form.Show();
        }
    }
}
