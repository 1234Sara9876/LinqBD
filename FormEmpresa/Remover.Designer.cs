﻿namespace FormEmpresa
{
    partial class Remover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Remover));
            this.btnFechar5 = new System.Windows.Forms.Button();
            this.btnGravar4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbRemover = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnFechar5
            // 
            this.btnFechar5.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar5.Image")));
            this.btnFechar5.Location = new System.Drawing.Point(294, 224);
            this.btnFechar5.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar5.Name = "btnFechar5";
            this.btnFechar5.Size = new System.Drawing.Size(33, 36);
            this.btnFechar5.TabIndex = 29;
            this.btnFechar5.UseVisualStyleBackColor = true;
            this.btnFechar5.Click += new System.EventHandler(this.btnFechar5_Click);
            // 
            // btnGravar4
            // 
            this.btnGravar4.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar4.Image")));
            this.btnGravar4.Location = new System.Drawing.Point(246, 224);
            this.btnGravar4.Margin = new System.Windows.Forms.Padding(2);
            this.btnGravar4.Name = "btnGravar4";
            this.btnGravar4.Size = new System.Drawing.Size(33, 36);
            this.btnGravar4.TabIndex = 28;
            this.btnGravar4.UseVisualStyleBackColor = true;
            this.btnGravar4.Click += new System.EventHandler(this.btnGravar4_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(61, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 19);
            this.label1.TabIndex = 27;
            this.label1.Text = "Seleccionar o funcionário";
            // 
            // cbRemover
            // 
            this.cbRemover.FormattingEnabled = true;
            this.cbRemover.Location = new System.Drawing.Point(40, 120);
            this.cbRemover.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemover.Name = "cbRemover";
            this.cbRemover.Size = new System.Drawing.Size(244, 21);
            this.cbRemover.TabIndex = 26;
            // 
            // Remover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 261);
            this.ControlBox = false;
            this.Controls.Add(this.btnFechar5);
            this.Controls.Add(this.btnGravar4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbRemover);
            this.Name = "Remover";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remover";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFechar5;
        private System.Windows.Forms.Button btnGravar4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbRemover;
    }
}