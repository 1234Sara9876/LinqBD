﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormEmpresa
{
    public partial class Criar : Form
    {
        private Funcionarios form;
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public Criar()
        {
            InitializeComponent();
            tbIDFunc.Text = GerarID().ToString();
        }

        private int GerarID()
        {
            var lista = from Funcionario in dc.Funcionarios select Funcionario;
            if (lista.Count() > 0)
                return lista.Count() + 1; 
            else
                return 1;
        }

        private void btnGravar2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbNomeFunc.Text))
            {
                MessageBox.Show("Tem que inserir o nome do funcionário");
                return;
            }
            if (string.IsNullOrEmpty(tbDep.Text))
            {
                MessageBox.Show("Tem que inserir a sigla do departamento");
                return;
            }

            if ( tbDep.Text != "DC" && tbDep.Text != "DF" && tbDep.Text != "RH" )
            {
                MessageBox.Show("Este departamento não existe");
                return;
            }

            Funcionario func = new Funcionario
            {
                ID = Convert.ToInt16(tbIDFunc.Text),
                Nome = tbNomeFunc.Text,
                Departamento = tbDep.Text
            };

            dc.Funcionarios.InsertOnSubmit(func);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
            MessageBox.Show("Novo funcionário gravado com sucesso");
            Close();
        }

        private void btnFechar3_Click(object sender, EventArgs e)
        {
            form = new Funcionarios();
            this.Close();
            form.Show();
        }
    }
}
