﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormEmpresa
{
    public partial class Remover : Form
    {
        private Funcionarios form;
        DataClasses1DataContext dc = new DataClasses1DataContext();
        
        public Remover()
        {
            InitializeComponent();
            InitCombo();
           
        }

        private void InitCombo()
        {
            var lista = from Funcionario in dc.Funcionarios select Funcionario;
            foreach (Funcionario func in lista)
            {
                cbRemover.DataSource = lista;
                cbRemover.DisplayMember = "Nome";
            }
        }

        private void btnGravar4_Click(object sender, EventArgs e)
        {
            if (cbRemover.SelectedIndex == -1)
            {
                MessageBox.Show("Indique o funcionário");
                return;
            }
            Funcionario f = new Funcionario();
            var lista = from Funcionario in dc.Funcionarios
                        where Funcionario.Nome == cbRemover.Text
                        select Funcionario;

            f = lista.Single();
            f.Nome = "Inativo";

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
            MessageBox.Show("Funcionário apagado da lista");
            Close();
        }

        private void btnFechar5_Click(object sender, EventArgs e)
        {
            form = new Funcionarios();
            this.Close();
            form.Show();
           
        }
    }
}

