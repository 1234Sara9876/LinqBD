﻿namespace FormEmpresa
{
    partial class Departamentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Departamentos));
            this.label2 = new System.Windows.Forms.Label();
            this.tbStatusDep = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDep = new System.Windows.Forms.ComboBox();
            this.btnFechar1 = new System.Windows.Forms.Button();
            this.btnCreate1 = new System.Windows.Forms.Button();
            this.btnGravar1 = new System.Windows.Forms.Button();
            this.tbNovoDep = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSigla = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(172, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Ativo/Inativo";
            // 
            // tbStatusDep
            // 
            this.tbStatusDep.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbStatusDep.Location = new System.Drawing.Point(175, 107);
            this.tbStatusDep.Name = "tbStatusDep";
            this.tbStatusDep.Size = new System.Drawing.Size(75, 22);
            this.tbStatusDep.TabIndex = 21;
            this.tbStatusDep.Text = "Ativo";
            this.tbStatusDep.TextChanged += new System.EventHandler(this.tbStatusDep_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(131, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Sigla e nome de departamento";
            // 
            // cbDep
            // 
            this.cbDep.FormattingEnabled = true;
            this.cbDep.Location = new System.Drawing.Point(72, 42);
            this.cbDep.Name = "cbDep";
            this.cbDep.Size = new System.Drawing.Size(304, 21);
            this.cbDep.TabIndex = 19;
            // 
            // btnFechar1
            // 
            this.btnFechar1.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar1.Image")));
            this.btnFechar1.Location = new System.Drawing.Point(398, 338);
            this.btnFechar1.Name = "btnFechar1";
            this.btnFechar1.Size = new System.Drawing.Size(48, 41);
            this.btnFechar1.TabIndex = 18;
            this.btnFechar1.UseVisualStyleBackColor = true;
            this.btnFechar1.Click += new System.EventHandler(this.btnFechar1_Click);
            // 
            // btnCreate1
            // 
            this.btnCreate1.Image = ((System.Drawing.Image)(resources.GetObject("btnCreate1.Image")));
            this.btnCreate1.Location = new System.Drawing.Point(175, 165);
            this.btnCreate1.Name = "btnCreate1";
            this.btnCreate1.Size = new System.Drawing.Size(75, 69);
            this.btnCreate1.TabIndex = 14;
            this.btnCreate1.UseVisualStyleBackColor = true;
            this.btnCreate1.Click += new System.EventHandler(this.btnCreate1_Click);
            // 
            // btnGravar1
            // 
            this.btnGravar1.Enabled = false;
            this.btnGravar1.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar1.Image")));
            this.btnGravar1.Location = new System.Drawing.Point(330, 338);
            this.btnGravar1.Name = "btnGravar1";
            this.btnGravar1.Size = new System.Drawing.Size(46, 41);
            this.btnGravar1.TabIndex = 23;
            this.btnGravar1.UseVisualStyleBackColor = true;
            this.btnGravar1.Click += new System.EventHandler(this.btnGravar1_Click);
            // 
            // tbNovoDep
            // 
            this.tbNovoDep.Enabled = false;
            this.tbNovoDep.Location = new System.Drawing.Point(134, 281);
            this.tbNovoDep.Name = "tbNovoDep";
            this.tbNovoDep.Size = new System.Drawing.Size(242, 20);
            this.tbNovoDep.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(193, 265);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Novo Departamento";
            // 
            // tbSigla
            // 
            this.tbSigla.Enabled = false;
            this.tbSigla.Location = new System.Drawing.Point(72, 281);
            this.tbSigla.Name = "tbSigla";
            this.tbSigla.ReadOnly = true;
            this.tbSigla.Size = new System.Drawing.Size(56, 20);
            this.tbSigla.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(83, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Sigla";
            // 
            // Departamentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 380);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbSigla);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbNovoDep);
            this.Controls.Add(this.btnGravar1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbStatusDep);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbDep);
            this.Controls.Add(this.btnFechar1);
            this.Controls.Add(this.btnCreate1);
            this.Name = "Departamentos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Departamentos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbStatusDep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDep;
        private System.Windows.Forms.Button btnFechar1;
        private System.Windows.Forms.Button btnCreate1;
        private System.Windows.Forms.Button btnGravar1;
        private System.Windows.Forms.TextBox tbNovoDep;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbSigla;
        private System.Windows.Forms.Label label4;
    }
}