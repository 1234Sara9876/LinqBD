﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormEmpresa
{
    public partial class Ver : Form
    {
        private Funcionarios form;
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public Ver()
        {
            InitializeComponent();
            InitCombo();
        }

        private void InitCombo()
        {
            var lista = from Funcionario in dc.Funcionarios select Funcionario;
            cbFunc.DataSource = lista;
            cbFunc.DisplayMember = "Nome";
        }

        private void cbFunc_SelectedIndexChanged(object sender, EventArgs e)
        {
            Funcionario funcSeleccionado = (Funcionario)cbFunc.SelectedItem;
            
            //listview.View = View.Details; - isto está feito em Properties

            //LISTVIEW
            lviewFiltro.Clear();

            //preencher os headers da listview
            lviewFiltro.Columns.Add("Id");
            lviewFiltro.Columns.Add("Nome");
            lviewFiltro.Columns.Add("Departamento");

            var lista = from Funcionario in dc.Funcionarios
                        where Funcionario == funcSeleccionado
                        select Funcionario;

            //preencher o conteúdo das colunas
            foreach (Funcionario func in lista)
            {
                ListViewItem line; //cria o objeto de form "linha de listview"
                line = lviewFiltro.Items.Add(func.ID.ToString());
                //atribuiu a 1ª coluna da tabela à 1ª coluna da listview em formato string
                line.SubItems.Add(func.Nome);
                line.SubItems.Add(func.Departamento);
                //atribuiu os campos seguintes do mesmo registo (da esquerda para a direita)
            }

            for (int idx = 0; idx <= 2; idx++)
            {
                lviewFiltro.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        private void tbFunc_TextChanged(object sender, EventArgs e)
        {
            var depSeleccionado = tbFunc.Text;

            lviewFiltro.Clear();

            //preencher os headers da listview
            lviewFiltro.Columns.Add("Id");
            lviewFiltro.Columns.Add("Nome");
            lviewFiltro.Columns.Add("Departamento");

            var lista = from Funcionario in dc.Funcionarios
                        where Funcionario.Departamento == depSeleccionado
                        select Funcionario;

            foreach (Funcionario func in lista)
            {
                ListViewItem line; //cria o objeto de form "linha de listview"
                line = lviewFiltro.Items.Add(func.ID.ToString());
                //atribuiu a 1ª coluna da tabela à 1ª coluna da listview em formato string
                line.SubItems.Add(func.Nome);
                line.SubItems.Add(func.Departamento);
                //atribuiu os campos seguintes do mesmo registo (da esquerda para a direita)
            }

            for (int idx = 0; idx <= 2; idx++)
            {
                lviewFiltro.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }

        private void Ver_Load(object sender, EventArgs e)
        {
            //GRIDVIEW
            var lista = from Funcionario in dc.Funcionarios select Funcionario;
            //preencher headers
            dgviewFunc.Columns.Add("colId", "ID");
            dgviewFunc.Columns.Add("colNome", "Nome");
            dgviewFunc.Columns.Add("colDepartamento", "Departamento");

            int idxlinha = 0;

            //método de atribuir cor ao dataGridView
            DataGridViewCellStyle estiloDC = new DataGridViewCellStyle();
            estiloDC.ForeColor = Color.Plum;
            estiloDC.BackColor = Color.LightSkyBlue;

            foreach (Funcionario func in lista)
            {
                DataGridViewRow linha = new DataGridViewRow();
                dgviewFunc.Rows.Add(linha);

                dgviewFunc.Rows[idxlinha].Cells[0].Value = func.ID;
                dgviewFunc.Rows[idxlinha].Cells[1].Value = func.Nome;
                dgviewFunc.Rows[idxlinha].Cells[2].Value = func.Departamento;

                if ((string)dgviewFunc.Rows[idxlinha].Cells[2].Value == "DC")
                {
                    dgviewFunc.Rows[idxlinha].DefaultCellStyle = estiloDC;
                }
                idxlinha++;

            }
            //ajusta as colunas à gridview
            dgviewFunc.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void btnFechar4_Click(object sender, EventArgs e)
        {
            form = new Funcionarios();
            this.Close();
            form.Show();
           
        }

       
    }
}
    

