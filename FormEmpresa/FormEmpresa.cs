﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormEmpresa
{
    public partial class FormEmpresa : Form
    {
        private Funcionarios formFunc;
        private Departamentos formDep; 

        public FormEmpresa()
        {
            InitializeComponent();
        }

        private void btnDep_Click(object sender, EventArgs e)
        {
            formDep = new Departamentos();
            Hide();
            formDep.Show();
        }

        private void btnFunc_Click(object sender, EventArgs e)
        {
            formFunc = new Funcionarios();
            Hide();
            formFunc.Show();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
