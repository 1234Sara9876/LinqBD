﻿namespace FormDepartamentos
{
    partial class FormDepartamentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDepartamentos));
            this.btnFechar1 = new System.Windows.Forms.Button();
            this.btnDelete1 = new System.Windows.Forms.Button();
            this.btnUpdate1 = new System.Windows.Forms.Button();
            this.btnRead1 = new System.Windows.Forms.Button();
            this.btnCreate1 = new System.Windows.Forms.Button();
            this.cbDep = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbNovoDep = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnFechar1
            // 
            this.btnFechar1.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar1.Image")));
            this.btnFechar1.Location = new System.Drawing.Point(347, 330);
            this.btnFechar1.Name = "btnFechar1";
            this.btnFechar1.Size = new System.Drawing.Size(48, 41);
            this.btnFechar1.TabIndex = 9;
            this.btnFechar1.UseVisualStyleBackColor = true;
            // 
            // btnDelete1
            // 
            this.btnDelete1.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete1.Image")));
            this.btnDelete1.Location = new System.Drawing.Point(210, 249);
            this.btnDelete1.Name = "btnDelete1";
            this.btnDelete1.Size = new System.Drawing.Size(75, 73);
            this.btnDelete1.TabIndex = 8;
            this.btnDelete1.UseVisualStyleBackColor = true;
            // 
            // btnUpdate1
            // 
            this.btnUpdate1.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate1.Image")));
            this.btnUpdate1.Location = new System.Drawing.Point(93, 249);
            this.btnUpdate1.Name = "btnUpdate1";
            this.btnUpdate1.Size = new System.Drawing.Size(75, 73);
            this.btnUpdate1.TabIndex = 7;
            this.btnUpdate1.UseVisualStyleBackColor = true;
            // 
            // btnRead1
            // 
            this.btnRead1.Image = ((System.Drawing.Image)(resources.GetObject("btnRead1.Image")));
            this.btnRead1.Location = new System.Drawing.Point(210, 149);
            this.btnRead1.Name = "btnRead1";
            this.btnRead1.Size = new System.Drawing.Size(75, 69);
            this.btnRead1.TabIndex = 6;
            this.btnRead1.UseVisualStyleBackColor = true;
            // 
            // btnCreate1
            // 
            this.btnCreate1.Image = ((System.Drawing.Image)(resources.GetObject("btnCreate1.Image")));
            this.btnCreate1.Location = new System.Drawing.Point(93, 149);
            this.btnCreate1.Name = "btnCreate1";
            this.btnCreate1.Size = new System.Drawing.Size(75, 69);
            this.btnCreate1.TabIndex = 5;
            this.btnCreate1.UseVisualStyleBackColor = true;
            // 
            // cbDep
            // 
            this.cbDep.FormattingEnabled = true;
            this.cbDep.Location = new System.Drawing.Point(47, 40);
            this.cbDep.Name = "cbDep";
            this.cbDep.Size = new System.Drawing.Size(304, 21);
            this.cbDep.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(106, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Sigla e nome de departamento";
            // 
            // tbNovoDep
            // 
            this.tbNovoDep.Location = new System.Drawing.Point(47, 105);
            this.tbNovoDep.Name = "tbNovoDep";
            this.tbNovoDep.Size = new System.Drawing.Size(304, 20);
            this.tbNovoDep.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(137, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Novo Departamento";
            // 
            // FormDepartamentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 371);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNovoDep);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbDep);
            this.Controls.Add(this.btnFechar1);
            this.Controls.Add(this.btnDelete1);
            this.Controls.Add(this.btnUpdate1);
            this.Controls.Add(this.btnRead1);
            this.Controls.Add(this.btnCreate1);
            this.Name = "FormDepartamentos";
            this.Text = "Departamentos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFechar1;
        private System.Windows.Forms.Button btnDelete1;
        private System.Windows.Forms.Button btnUpdate1;
        private System.Windows.Forms.Button btnRead1;
        private System.Windows.Forms.Button btnCreate1;
        private System.Windows.Forms.ComboBox cbDep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNovoDep;
        private System.Windows.Forms.Label label2;
    }
}

