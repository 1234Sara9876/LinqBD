﻿namespace FormEmpresa
{
    partial class Editar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editar));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDepEdit = new System.Windows.Forms.TextBox();
            this.tbNomeFuncEdit = new System.Windows.Forms.TextBox();
            this.tbIDFuncEdit = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGravar3 = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 228);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Departamento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "Nome completo";
            // 
            // tbDepEdit
            // 
            this.tbDepEdit.Location = new System.Drawing.Point(131, 228);
            this.tbDepEdit.Name = "tbDepEdit";
            this.tbDepEdit.Size = new System.Drawing.Size(187, 20);
            this.tbDepEdit.TabIndex = 38;
            // 
            // tbNomeFuncEdit
            // 
            this.tbNomeFuncEdit.Location = new System.Drawing.Point(25, 111);
            this.tbNomeFuncEdit.Multiline = true;
            this.tbNomeFuncEdit.Name = "tbNomeFuncEdit";
            this.tbNomeFuncEdit.Size = new System.Drawing.Size(296, 63);
            this.tbNomeFuncEdit.TabIndex = 37;
            // 
            // tbIDFuncEdit
            // 
            this.tbIDFuncEdit.BackColor = System.Drawing.SystemColors.Window;
            this.tbIDFuncEdit.Location = new System.Drawing.Point(183, 25);
            this.tbIDFuncEdit.Margin = new System.Windows.Forms.Padding(2);
            this.tbIDFuncEdit.Name = "tbIDFuncEdit";
            this.tbIDFuncEdit.ReadOnly = true;
            this.tbIDFuncEdit.Size = new System.Drawing.Size(40, 20);
            this.tbIDFuncEdit.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "ID Funcionário";
            // 
            // btnGravar3
            // 
            this.btnGravar3.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar3.Image")));
            this.btnGravar3.Location = new System.Drawing.Point(335, 270);
            this.btnGravar3.Margin = new System.Windows.Forms.Padding(2);
            this.btnGravar3.Name = "btnGravar3";
            this.btnGravar3.Size = new System.Drawing.Size(33, 36);
            this.btnGravar3.TabIndex = 34;
            this.btnGravar3.UseVisualStyleBackColor = true;
            this.btnGravar3.Click += new System.EventHandler(this.btnGravar3_Click);
            // 
            // btnForward
            // 
            this.btnForward.Image = ((System.Drawing.Image)(resources.GetObject("btnForward.Image")));
            this.btnForward.Location = new System.Drawing.Point(168, 270);
            this.btnForward.Margin = new System.Windows.Forms.Padding(2);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(26, 36);
            this.btnForward.TabIndex = 33;
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // btnBack
            // 
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(79, 270);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(23, 36);
            this.btnBack.TabIndex = 32;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // Editar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 306);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDepEdit);
            this.Controls.Add(this.tbNomeFuncEdit);
            this.Controls.Add(this.tbIDFuncEdit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGravar3);
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.btnBack);
            this.Name = "Editar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDepEdit;
        private System.Windows.Forms.TextBox tbNomeFuncEdit;
        private System.Windows.Forms.TextBox tbIDFuncEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGravar3;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBack;
    }
}