﻿namespace FormEmpresa
{
    partial class Ver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ver));
            this.tbFunc = new System.Windows.Forms.TextBox();
            this.btnFechar4 = new System.Windows.Forms.Button();
            this.dgviewFunc = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFunc = new System.Windows.Forms.ComboBox();
            this.lblTodosAlunos = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lviewFiltro = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.dgviewFunc)).BeginInit();
            this.SuspendLayout();
            // 
            // tbFunc
            // 
            this.tbFunc.Location = new System.Drawing.Point(25, 206);
            this.tbFunc.Name = "tbFunc";
            this.tbFunc.Size = new System.Drawing.Size(30, 20);
            this.tbFunc.TabIndex = 46;
            this.tbFunc.TextChanged += new System.EventHandler(this.tbFunc_TextChanged);
            // 
            // btnFechar4
            // 
            this.btnFechar4.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar4.Image")));
            this.btnFechar4.Location = new System.Drawing.Point(788, 441);
            this.btnFechar4.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar4.Name = "btnFechar4";
            this.btnFechar4.Size = new System.Drawing.Size(36, 33);
            this.btnFechar4.TabIndex = 45;
            this.btnFechar4.UseVisualStyleBackColor = true;
            this.btnFechar4.Click += new System.EventHandler(this.btnFechar4_Click);
            // 
            // dgviewFunc
            // 
            this.dgviewFunc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgviewFunc.Location = new System.Drawing.Point(26, 304);
            this.dgviewFunc.Name = "dgviewFunc";
            this.dgviewFunc.RowTemplate.Height = 24;
            this.dgviewFunc.Size = new System.Drawing.Size(730, 150);
            this.dgviewFunc.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(385, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Filtro de funcionários";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(239, 80);
            this.label1.TabIndex = 41;
            this.label1.Text = "Inserir a sigla do departamento:\r\n\r\nDC - Comercial\r\nDF - Financeiro\r\nRH - Recurso" +
    "s Humanos\r\n";
            // 
            // cbFunc
            // 
            this.cbFunc.FormattingEnabled = true;
            this.cbFunc.Location = new System.Drawing.Point(25, 48);
            this.cbFunc.Name = "cbFunc";
            this.cbFunc.Size = new System.Drawing.Size(236, 21);
            this.cbFunc.TabIndex = 40;
            this.cbFunc.SelectedIndexChanged += new System.EventHandler(this.cbFunc_SelectedIndexChanged);
            // 
            // lblTodosAlunos
            // 
            this.lblTodosAlunos.AutoSize = true;
            this.lblTodosAlunos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTodosAlunos.Location = new System.Drawing.Point(22, 28);
            this.lblTodosAlunos.Name = "lblTodosAlunos";
            this.lblTodosAlunos.Size = new System.Drawing.Size(141, 13);
            this.lblTodosAlunos.TabIndex = 39;
            this.lblTodosAlunos.Text = "Seleccionar funcionário";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 286);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 15);
            this.label3.TabIndex = 47;
            this.label3.Text = "Todos os funcionários";
            // 
            // lviewFiltro
            // 
            this.lviewFiltro.Location = new System.Drawing.Point(328, 48);
            this.lviewFiltro.Name = "lviewFiltro";
            this.lviewFiltro.Size = new System.Drawing.Size(428, 178);
            this.lviewFiltro.TabIndex = 48;
            this.lviewFiltro.UseCompatibleStateImageBehavior = false;
            this.lviewFiltro.View = System.Windows.Forms.View.Details;
            // 
            // Ver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 476);
            this.ControlBox = false;
            this.Controls.Add(this.lviewFiltro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbFunc);
            this.Controls.Add(this.btnFechar4);
            this.Controls.Add(this.dgviewFunc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbFunc);
            this.Controls.Add(this.lblTodosAlunos);
            this.Name = "Ver";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ver";
            this.Load += new System.EventHandler(this.Ver_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgviewFunc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFunc;
        private System.Windows.Forms.Button btnFechar4;
        private System.Windows.Forms.DataGridView dgviewFunc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFunc;
        private System.Windows.Forms.Label lblTodosAlunos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lviewFiltro;
    }
}