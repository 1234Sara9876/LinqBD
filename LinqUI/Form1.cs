﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqUI
{
    public partial class Form1 : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();
        //dc é uma variável q indica qual tabela da BD vamos utilizar
        bool upgoID = false;
        bool upgoNome = false; 
        bool upgoDep = false;
        //true está por ordem ascendente, i.é alfabética

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //o load é feito antes de inicializar a parte gráfica

            //LISTVIEW

            //preencher os headers 
            listView1.Columns.Add("Id");
            listView1.Columns.Add("Nome");
            listView1.Columns.Add("Departamento");

            //carregar dados
            var lista = from Funcionario in dc.Funcionarios select Funcionario;

            //preencher o conteúdo das colunas
            foreach( Funcionario func in lista)
            {
                ListViewItem line; //cria o objeto de form "linha de listview"
                line = listView1.Items.Add(func.ID.ToString());
                //atribuiu a 1ª coluna da tabela à 1ª coluna da listview em formato string
                line.SubItems.Add(func.Nome);
                line.SubItems.Add(func.Departamento);
                //atribuiu os campos seguintes do mesmo registo (da esquerda para a direita)
            }

            for (int idx = 0; idx<=2; idx++)
            {
                listView1.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }

            //TREEVIEW

            var outraLista = from Departamento in dc.Departamentos select Departamento;
            //segundo nível da árvore - Funcionários
            var outraLista2 = from Funcionario in dc.Funcionarios
                              orderby Funcionario.Nome
                              select Funcionario;

            foreach (Departamento dep in outraLista)
            {
                treeView1.Nodes.Add(dep.Sigla);
            }
            string departamento;

            foreach (Funcionario func in outraLista2)
            {
                departamento = func.Departamento;
                foreach (TreeNode principal in treeView1.Nodes)
                {
                    if (principal.Text == departamento)
                    {
                        principal.Nodes.Add(func.ID + " " +func.Nome);
                    }
                }
            }

            //GRIDVIEWS

            //preencher headers
            dataGridView1.Columns.Add("colId", "ID");
            dataGridView1.Columns.Add("colNome", "Nome");
            dataGridView1.Columns.Add("colDepartamento", "Departamento");

            var outraLista3= from Funcionario in dc.Funcionarios select Funcionario;

            int idxlinha = 0;

            //método de atribuir cor ao dataGridView
            DataGridViewCellStyle estiloDC = new DataGridViewCellStyle();
            estiloDC.ForeColor = Color.Red;
            estiloDC.BackColor = Color.Yellow;

            foreach (Funcionario func in outraLista3)
            {
                DataGridViewRow linha = new DataGridViewRow();
                dataGridView1.Rows.Add(linha);

                dataGridView1.Rows[idxlinha].Cells[0].Value = func.ID;
                dataGridView1.Rows[idxlinha].Cells[1].Value = func.Nome;
                dataGridView1.Rows[idxlinha].Cells[2].Value = func.Departamento;

                if ((string)dataGridView1.Rows[idxlinha].Cells[2].Value == "DC")
                {
                    dataGridView1.Rows[idxlinha].DefaultCellStyle = estiloDC;
                }
               
                idxlinha++;
            }
            //ajusta as colunas por código
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            //ajusta as colunas à gridview
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            int col = e.Column;//do columnClicksEventArgs e, que dá a posição da coluna
          
            switch (col)
            {
                case 0:
                    if (upgoID)
                    {
                        listView1.Items.Clear();
                        var listaOrdemID = from Funcionario in dc.Funcionarios
                                           orderby Funcionario.ID descending
                                           select Funcionario;

                        foreach (Funcionario func in listaOrdemID)
                        {
                            ListViewItem line;
                            line = listView1.Items.Add(func.ID.ToString());
                            line.SubItems.Add(func.Nome);
                            line.SubItems.Add(func.Departamento);
                        }
                        upgoID = false;
                    }
                    else
                    {
                        listView1.Items.Clear();
                        var listaOrdemID = from Funcionario in dc.Funcionarios
                                           orderby Funcionario.ID 
                                           select Funcionario;

                        foreach (Funcionario func in listaOrdemID)
                        {
                            ListViewItem line;
                            line = listView1.Items.Add(func.ID.ToString());
                            line.SubItems.Add(func.Nome);
                            line.SubItems.Add(func.Departamento);
                        }
                        upgoID = true;
                    }
                    break;

                case 1:
                    if (upgoNome)
                    {
                        listView1.Items.Clear();
                        var listaOrdemNome = from Funcionario in dc.Funcionarios
                                           orderby Funcionario.Nome descending
                                           select Funcionario;

                        foreach (Funcionario func in listaOrdemNome)
                        {
                            ListViewItem line;
                            line = listView1.Items.Add(func.ID.ToString());
                            line.SubItems.Add(func.Nome);
                            line.SubItems.Add(func.Departamento);
                        }
                        upgoNome = false;
                    }
                    else
                    {
                        listView1.Items.Clear();
                        var listaOrdemNome = from Funcionario in dc.Funcionarios
                                           orderby Funcionario.Nome
                                           select Funcionario;

                        foreach (Funcionario func in listaOrdemNome)
                        {
                            ListViewItem line;
                            line = listView1.Items.Add(func.ID.ToString());
                            line.SubItems.Add(func.Nome);
                            line.SubItems.Add(func.Departamento);
                        }
                        upgoNome = true;
                    }
                    break;

                case 2:
                    if (upgoDep)
                    {
                        listView1.Items.Clear();
                        var listaOrdemDep = from Funcionario in dc.Funcionarios
                                             orderby Funcionario.Departamento descending
                                             select Funcionario;

                        foreach (Funcionario func in listaOrdemDep)
                        {
                            ListViewItem line;
                            line = listView1.Items.Add(func.ID.ToString());
                            line.SubItems.Add(func.Nome);
                            line.SubItems.Add(func.Departamento);
                        }
                        upgoDep = false;
                    }
                    else
                    {
                        listView1.Items.Clear();
                        var listaOrdemDep = from Funcionario in dc.Funcionarios
                                             orderby Funcionario.Departamento
                                             select Funcionario;

                        foreach (Funcionario func in listaOrdemDep)
                        {
                            ListViewItem line;
                            line = listView1.Items.Add(func.ID.ToString());
                            line.SubItems.Add(func.Nome);
                            line.SubItems.Add(func.Departamento);
                        }
                        upgoDep = true;
                    }
                    break;
            }

        }
    }
}
